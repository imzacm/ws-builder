# WS Builder
[![pipeline status](https://gitlab.com/imzacm/ws-builder/badges/master/pipeline.svg)](https://gitlab.com/imzacm/ws-builder/commits/master)

[![coverage report](https://gitlab.com/imzacm/ws-builder/badges/master/coverage.svg)](https://gitlab.com/imzacm/ws-builder/commits/master)

Easily build a WebSocket API - uses ws

The idea of WS Builder is to use plugins to interact with WebSockets.

## Example
See example directory: [gitlab](https://gitlab.com/imzacm/ws-builder/tree/master/example)

Another example is: [UP Down Site](https://gitlab.com/imzacm/up-down-site)

## WsServer
The WsServer class creates an instance of ```WS.Server``` using the ```opts``` and ```startedCb``` passed into it.

The following scenarios will invoke all plugins in the plugins array in order:
* connection - plugins will be invoked with ```{'action': 'connect'}```
* message - plugins will be invoked with ```{'action': 'message'}```
* close of connection - plugins will be invoked with ```{'action': 'close'}```
* server error - plugins will be invoked with ```{'action': 'serverError'}```
* socket error - plugins will be invoked with ```{'action': 'clientError'}```
* plugin error - plugins will be invoked with ```{'action': 'pluginError'}```

```typescript
class WsServer {
  public readonly server: WS.Server
  public plugins: Plugin[]

  public constructor(opts?: WS.ServerOptions, startedCb?: () => void)
}
```

## Plugin
A plugin is a class that implements the ```Plugin``` interface.

```typescript
type pluginArgs = {
  ws: WS
  action: 'connect' | 'message' | 'close' | 'serverError' | 'clientError' | 'pluginError'
  data: any
}

interface Plugin {
  handle(args: pluginArgs): void
}
```

## Premade plugins
Currently the following plugins exist inside this module:

### JsonParser
Json parser is a plugin that will run every message through ```JSON.parse```, any plugin after JsonParser will receive data as an object if it was originally stringified JSON.

#### Usage
```typescript
// TS
import { WsServer, JsonParser } from 'ws-builder'
// JS
const { WsServer, JsonParser } = require('ws-builder')

const parser = new JsonParser(true) // true to error on invalid JSON, false catches the error

const server = new WsServer({ port: 3005 })
server.plugins.push(parser)

// Send a message to server
import * as WS from 'ws'
const WS = require('ws')

const ws = new WS('ws://localhost:3005')
ws.on('open', () => {
  ws.send(JSON.stringify({
    key: 'value'
  }))
})
```

### KeyRouter
Key Router is a routing plugin, it contains an object called ```keys``` which is a key/value pair or route keys to handlerFunctions.

#### Usage
```typescript
// TS
import { WsServer, KeyRouter } from 'ws-builder'
// JS
const { WsServer, KeyRouter } = require('ws-builder')

// param 1: true to throw error if message body does not contain the 'key' key
// param 2: true to throw error if value of 'key' is not in the 'keys' object
// param 3: true to add 'help' key which will send an array of keys
const router = new KeyRouter(true, true, true)

router.keys.value = (ws: WS, msg: WS.Data) => {
  // Do something with msg and send response with ws.send

  // msg: { key: 'value', data: { some: 'kind of data' } }
}

const server = new WsServer({ port: 3005 })
server.plugins.push(router)

// Send a message to server
import * as WS from 'ws'
const WS = require('ws')

const ws = new WS('ws://localhost:3005')
ws.on('open', () => {
  ws.send(JSON.stringify({
    key: 'value',
    data: {
      some: 'kind of data'
    }
  }))
})
```

### Logger
Logger simply logs the data passed to it using a function set in it's logFuncs object.

#### Usage
```typescript
// TS
import { WsServer, Logger } from 'ws-builder'
// JS
const { WsServer, Logger } = require('ws-builder')

const logger = new Logger()
logger.logFuncs = {
  log: logFunction, // Defaults to console.log, will be used for any logging that is not an error
  warn: warnFunction, // Defaults to console.warn, currently will never be used
  error: errorFunction // Defaults to console.error, will be used when handler is called with { action: '*Error' } (where * is a wildcard)
}

const server = new WsServer({ port: 3005 })
server.plugins.push(logger)

// Send a message to server
import * as WS from 'ws'
const WS = require('ws')

const ws = new WS('ws://localhost:3005')
ws.on('open', () => {
  ws.send(JSON.stringify({
    test: true
  }))
})
```