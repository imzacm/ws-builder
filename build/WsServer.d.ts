import * as WS from 'ws';
import { pluginArgs, Plugin } from './';
export declare class WsServer {
    readonly server: WS.Server;
    plugins: Plugin[];
    constructor(opts?: WS.ServerOptions, startedCb?: () => void);
    protected _callPlugins(data: pluginArgs): void;
    protected _serverErrorHandler(error: Error): void;
    protected _clientErrorHandler(ws: WS, error: Error): void;
    protected _connectionHandler(ws: WS): void;
    protected _messageHandler(ws: WS, msg: WS.Data): void;
    protected _closeHandler(ws: WS, code: number, reason: string): void;
}
