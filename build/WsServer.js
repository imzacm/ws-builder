"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const WS = require("ws");
class WsServer {
    constructor(opts, startedCb) {
        this.plugins = [];
        this.server = new WS.Server(opts, startedCb);
        this.server.on('error', this._serverErrorHandler.bind(this));
        this.server.on('connection', this._connectionHandler.bind(this));
    }
    _callPlugins(data) {
        const cb = (plug) => {
            try {
                plug.handle(data);
            }
            catch (e) {
                this._callPlugins({
                    ws: data.ws,
                    action: 'pluginError',
                    data: e
                });
            }
        };
        this.plugins.forEach(cb.bind(this));
    }
    _serverErrorHandler(error) {
        const data = {
            ws: null,
            action: 'serverError',
            data: error
        };
        this._callPlugins(data);
    }
    _clientErrorHandler(ws, error) {
        const data = {
            ws,
            action: 'clientError',
            data: error
        };
        this._callPlugins(data);
    }
    _connectionHandler(ws) {
        const msgProxy = (msg) => this._messageHandler(ws, msg);
        const errorProxy = (error) => this._clientErrorHandler(ws, error);
        const closeProxy = (code, reason) => this._closeHandler(ws, code, reason);
        ws.on('message', msgProxy.bind(this));
        ws.on('error', errorProxy.bind(this));
        ws.on('close', closeProxy.bind(this));
        const data = {
            ws,
            action: 'connect',
            data: null
        };
        this._callPlugins(data);
    }
    _messageHandler(ws, msg) {
        const data = {
            ws,
            action: 'message',
            data: msg
        };
        this._callPlugins(data);
    }
    _closeHandler(ws, code, reason) {
        const data = {
            ws,
            action: 'close',
            data: { code, reason }
        };
        this._callPlugins(data);
    }
}
exports.WsServer = WsServer;
//# sourceMappingURL=WsServer.js.map