import * as WS from 'ws';
export * from './plugins';
export * from './WsServer';
export declare type pluginArgs = {
    ws: WS;
    action: 'connect' | 'message' | 'close' | 'serverError' | 'clientError' | 'pluginError';
    data: any;
};
export interface Plugin {
    handle(args: pluginArgs): void;
}
