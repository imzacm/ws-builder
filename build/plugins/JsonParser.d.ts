import { Plugin, pluginArgs } from '../';
export declare class JsonParser implements Plugin {
    errorOnInvalidJson: boolean;
    constructor(errorOnInvalidJson?: boolean);
    handle(data: pluginArgs): void;
}
