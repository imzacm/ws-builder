"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class JsonParser {
    constructor(errorOnInvalidJson = true) {
        this.errorOnInvalidJson = errorOnInvalidJson;
    }
    handle(data) {
        if (data.action !== 'message') {
            return;
        }
        const str = data.data;
        if (typeof str !== 'string') {
            return;
        }
        try {
            data.data = JSON.parse(str);
        }
        catch (e) {
            if (this.errorOnInvalidJson) {
                throw e;
            }
        }
    }
}
exports.JsonParser = JsonParser;
//# sourceMappingURL=JsonParser.js.map