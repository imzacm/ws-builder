import * as WS from 'ws';
import { Plugin, pluginArgs } from '../';
export declare type handler = (ws: WS, msg: WS.Data) => void;
export declare class KeyRouter implements Plugin {
    keys: {
        [key: string]: handler;
    };
    errorOnNoKey: boolean;
    errorOnInvalidKey: boolean;
    constructor(errorOnNoKey?: boolean, errorOnInvalidKey?: boolean, helpKey?: boolean);
    handle(data: pluginArgs): void;
    helpKey: boolean;
}
