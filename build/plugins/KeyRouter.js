"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class KeyRouter {
    constructor(errorOnNoKey = true, errorOnInvalidKey = true, helpKey = true) {
        this.keys = {};
        this.errorOnNoKey = errorOnNoKey;
        this.errorOnInvalidKey = errorOnInvalidKey;
        this.helpKey = helpKey;
    }
    handle(data) {
        if (data.action !== 'message') {
            return;
        }
        if (!data.data.key) {
            if (this.errorOnNoKey) {
                throw new Error(`No key in message body: ${data.data}`);
            }
            return;
        }
        if (!this.keys[data.data.key]) {
            if (this.errorOnInvalidKey) {
                throw new Error(`Invalid key: ${data.data}`);
            }
            return;
        }
        this.keys[data.data.key](data.ws, data.data);
    }
    set helpKey(value) {
        if (value) {
            this.keys.help = (ws) => {
                const keys = Object.keys(this.keys);
                ws.send(JSON.stringify(keys));
            };
        }
        else {
            delete this.keys.help;
        }
    }
    get helpKey() {
        return !!this.keys.help;
    }
}
exports.KeyRouter = KeyRouter;
//# sourceMappingURL=KeyRouter.js.map