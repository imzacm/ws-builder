import { Plugin, pluginArgs } from '../';
export declare class Logger implements Plugin {
    logFuncs: {
        log: {
            (message?: any, ...optionalParams: any[]): void;
            (message?: any, ...optionalParams: any[]): void;
        };
        warn: {
            (message?: any, ...optionalParams: any[]): void;
            (message?: any, ...optionalParams: any[]): void;
        };
        error: {
            (message?: any, ...optionalParams: any[]): void;
            (message?: any, ...optionalParams: any[]): void;
        };
    };
    handle(data: pluginArgs): void;
}
