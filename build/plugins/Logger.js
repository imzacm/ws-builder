"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Logger {
    constructor() {
        this.logFuncs = {
            log: console.log,
            warn: console.warn,
            error: console.error
        };
    }
    handle(data) {
        let logFunc = this.logFuncs.log;
        if (data.action.includes('Error')) {
            logFunc = this.logFuncs.error;
        }
        logFunc(data);
    }
}
exports.Logger = Logger;
//# sourceMappingURL=Logger.js.map