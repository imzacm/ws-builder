const WS = require('ws')

const ws = new WS('ws://localhost:3005')
ws.on('open', () => {
  ws.send(JSON.stringify({
    key: 'echo',
    data: {
      this: 'is some data'
    },
    other: 'data'
  }))
  ws.send(JSON.stringify({
    key: 'help'
  }))
})

ws.on('message', console.log)