const ws = new WebSocket('ws://localhost:3005')
ws.onopen = () => {
  ws.send(JSON.stringify({
    key: 'echo',
    data: {
      this: 'is some data'
    },
    other: 'data'
  }))
  ws.send(JSON.stringify({
    key: 'help'
  }))
}
ws.onmessage = console.log