const wsBuilder = require('../build')

const server = new wsBuilder.WsServer({
  port: 3005
})

const logger = new wsBuilder.Logger()
const parser = new wsBuilder.JsonParser(true)
const router = new wsBuilder.KeyRouter(true, true, true)
router.keys = {
  echo: (ws, msg) => {
    ws.send(JSON.stringify(msg))
  }
}

server.plugins = [
  logger,
  parser,
  router
]