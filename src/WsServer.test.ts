import 'mocha'
import { expect } from 'chai'

import * as WS from 'ws'
import { WsServer } from './WsServer'
import { Plugin, pluginArgs } from './'

const testPlugin = new class TestPlugin implements Plugin {
  public data: pluginArgs
  handle(data: pluginArgs) {
    this.data = data
    if (data.data === 'TestTest') {
      this.throw(new Error('Test'))
    }
  }
  reset() {
    this.data = null
  }
  throw(e: Error) {
    throw e
  }
}()

describe('WsServer', () => {
  let srv: WsServer

  beforeEach(() => {
    return new Promise(res => {
      srv = new WsServer({
        port: 3005
      }, res)
      srv.plugins.push(testPlugin)
    })
  })

  afterEach(() => {
    return new Promise((res, rej) => {
      srv.server.close(err => {
        return err ? rej(err) : res()
      })
      testPlugin.reset()
    })
  })

  describe('routes data to plugins on', () => {
    it('serverError', () => {
      const e = new Error('Test')
      srv.server.emit('error', e)
      expect(testPlugin.data).to.deep.equal({
        ws: null,
        action: 'serverError',
        data: e
      })
    })

    it('pluginError', () => {
      return new Promise(res => {
        const testWs = new WS('ws://localhost:3005')
          .on('open', () => {
            testWs.send('TestTest')
            setTimeout(() => {
              expect(testPlugin.data.action).to.equal('pluginError')
              expect(testPlugin.data.data).to.be.instanceof(Error)
              res()
            }, 5)
          })
      })
    })

    it('message', () => {
      return new Promise(res => {
        const testWs = new WS('ws://localhost:3005')
          .on('open', () => {
            testWs.send('this is a message')
            setTimeout(() => {
              expect(testPlugin.data.action).to.equal('message')
              expect(testPlugin.data.data).to.equal('this is a message')
              res()
            })
          })
      })
    })

    it('connect', () => {
      return new Promise(res => {
        new WS('ws://localhost:3005')
          .on('open', () => {
            expect(testPlugin.data.action).to.equal('connect')
            expect(testPlugin.data.data).to.equal(null)
            res()
          })
      })
    })

    it('close', () => {
      return new Promise(res => {
        const testWs = new WS('ws://localhost:3005')
          .on('open', () => {
            testWs.close()
            setTimeout(() => {
              expect(testPlugin.data.action).to.equal('close')
              expect(testPlugin.data.data).to.deep.equal({
                code: 1005,
                reason: ''
              })
              res()
            }, 5)
          })
      })
    })
  })
})