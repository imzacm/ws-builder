import * as WS from 'ws'
import { pluginArgs, Plugin } from './'

export class WsServer {
  public readonly server: WS.Server
  public plugins: Plugin[] = []

  public constructor(opts?: WS.ServerOptions, startedCb?: () => void) {
    this.server = new WS.Server(opts, startedCb)
    this.server.on('error', this._serverErrorHandler.bind(this))
    this.server.on('connection', this._connectionHandler.bind(this))
  }

  protected _callPlugins(data: pluginArgs) {
    const cb = (plug: Plugin) => {
      try {
        plug.handle(data)
      }
      catch (e) {
        this._callPlugins({
          ws: data.ws,
          action: 'pluginError',
          data: e
        })
      }
    }
    this.plugins.forEach(cb.bind(this))
  }

  protected _serverErrorHandler(error: Error) {
    const data: pluginArgs = {
      ws: null,
      action: 'serverError',
      data: error
    }
    this._callPlugins(data)
  }

  protected _clientErrorHandler(ws: WS, error: Error) {
    const data: pluginArgs = {
      ws,
      action: 'clientError',
      data: error
    }
    this._callPlugins(data)
  }

  protected _connectionHandler(ws: WS) {
    const msgProxy = (msg: WS.Data) => this._messageHandler(ws, msg)
    const errorProxy = (error: Error) => this._clientErrorHandler(ws, error)
    const closeProxy = (code: number, reason: string) => this._closeHandler(ws, code, reason)

    ws.on('message', msgProxy.bind(this))
    ws.on('error', errorProxy.bind(this))
    ws.on('close', closeProxy.bind(this))

    const data: pluginArgs = {
      ws,
      action: 'connect',
      data: null
    }
    this._callPlugins(data)
  }

  protected _messageHandler(ws: WS, msg: WS.Data) {
    const data: pluginArgs = {
      ws,
      action: 'message',
      data: msg
    }
    this._callPlugins(data)
  }

  protected _closeHandler(ws: WS, code: number, reason: string) {
    const data: pluginArgs = {
      ws,
      action: 'close',
      data: { code, reason }
    }
    this._callPlugins(data)
  }
}