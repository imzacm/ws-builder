import 'mocha'
import { expect } from 'chai'

import * as WS from 'ws'
import { WsServer } from '../WsServer'
import { JsonParser } from './'
import { Plugin, pluginArgs } from '../'

const testPlugin = new class TestPlugin implements Plugin {
  public data: pluginArgs
  handle(data: pluginArgs) {
    if (data.action.includes('Error') || data.action === 'message') {
      this.data = this.data || data
    }
  }
  reset() {
    this.data = null
  }
  throw(e: Error) {
    throw e
  }
}()

describe('JSON Parser', () => {
  let srv: WsServer

  beforeEach(() => {
    return new Promise(res => {
      srv = new WsServer({
        port: 3005
      }, res)
      srv.plugins.push(new JsonParser())
      srv.plugins.push(testPlugin)
    })
  })

  afterEach(() => {
    return new Promise((res, rej) => {
      srv.server.close(err => {
        return err ? rej(err) : res()
      })
      testPlugin.reset()
    })
  })

  it('parses a json message', () => {
    return new Promise(res => {
      const testWs = new WS('ws://localhost:3005')
        .on('open', () => {
          const msg = { test: true }
          testWs.send(JSON.stringify(msg))
          setTimeout(() => {
            expect(testPlugin.data.action).to.equal('message')
            expect(testPlugin.data.data).to.deep.equal(msg)
            res()
          }, 5)
        })
    })
  })

  it('throws an error on invalid JSON by default', () => {
    return new Promise(res => {
      const testWs = new WS('ws://localhost:3005')
        .on('open', () => {
          testWs.send('This is not JSON')
          setTimeout(() => {
            expect(testPlugin.data.action).to.equal('pluginError')
            expect(testPlugin.data.data).to.be.instanceof(Error)
            res()
          }, 5)
        })
    })
  })

  it('throws an error on invalid JSON when constructed with true', () => {
    return new Promise(res => {
      srv.plugins[0] = new JsonParser(true)
      const testWs = new WS('ws://localhost:3005')
        .on('open', () => {
          testWs.send('This is not JSON')
          setTimeout(() => {
            expect(testPlugin.data.action).to.equal('pluginError')
            expect(testPlugin.data.data).to.be.instanceof(Error)
            res()
          }, 5)
        })
    })
  })

  it('throws an error on invalid JSON when constructed with false', () => {
    return new Promise(res => {
      srv.plugins[0] = new JsonParser(false)
      const testWs = new WS('ws://localhost:3005')
        .on('open', () => {
          testWs.send('This is not JSON')
          setTimeout(() => {
            expect(testPlugin.data.action).to.equal('message')
            expect(testPlugin.data.data).to.equal('This is not JSON')
            res()
          }, 5)
        })
    })
  })
})