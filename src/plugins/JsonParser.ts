import * as WS from 'ws'
import { Plugin, pluginArgs } from '../'

export class JsonParser implements Plugin {
  public errorOnInvalidJson: boolean

  public constructor(errorOnInvalidJson: boolean = true) {
    this.errorOnInvalidJson = errorOnInvalidJson
  }

  public handle(data: pluginArgs) {
    if (data.action !== 'message') {
      return
    }
    const str = data.data
    if (typeof str !== 'string') {
      return
    }
    try {
      data.data = JSON.parse(str)
    }
    catch (e) {
      if (this.errorOnInvalidJson) {
        throw e
      }
    }
  }
}