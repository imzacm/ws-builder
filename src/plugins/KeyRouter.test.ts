import 'mocha'
import { expect } from 'chai'

import * as WS from 'ws'
import { WsServer } from '../WsServer'
import { KeyRouter, JsonParser } from './'
import { Plugin, pluginArgs } from '../'

const testPlugin = new class TestPlugin implements Plugin {
  public data: pluginArgs
  handle(data: pluginArgs) {
    if (data.action.includes('Error') || data.action === 'message') {
      this.data = this.data || data
    }
  }
  reset() {
    this.data = null
  }
  throw(e: Error) {
    throw e
  }
}()

describe('Key Router', () => {
  let srv: WsServer

  beforeEach(() => {
    return new Promise(res => {
      srv = new WsServer({
        port: 3005
      }, res)
      srv.plugins.push(new JsonParser())
      srv.plugins.push(new KeyRouter())
      srv.plugins.push(testPlugin)
    })
  })

  afterEach(() => {
    return new Promise((res, rej) => {
      srv.server.close(err => {
        return err ? rej(err) : res()
      })
      testPlugin.reset()
    })
  })

  it('throws an error on no key by default', () => {
    return new Promise(res => {
      const testWs = new WS('ws://localhost:3005')
        .on('open', () => {
          const msg = {}
          testWs.send(JSON.stringify(msg))
          setTimeout(() => {
            expect(testPlugin.data.action).to.equal('pluginError')
            expect(testPlugin.data.data).to.be.instanceof(Error)
            res()
          }, 5)
        })
    })
  })

  it('throws an error on no key when constructed with true', () => {
    return new Promise(res => {
      srv.plugins[1] = new KeyRouter(true)
      const testWs = new WS('ws://localhost:3005')
        .on('open', () => {
          const msg = {}
          testWs.send(JSON.stringify(msg))
          setTimeout(() => {
            expect(testPlugin.data.action).to.equal('pluginError')
            expect(testPlugin.data.data).to.be.instanceof(Error)
            res()
          }, 5)
        })
    })
  })

  it('does not throw an error on no key when constructed with false', () => {
    return new Promise(res => {
      srv.plugins[1] = new KeyRouter(false)
      const testWs = new WS('ws://localhost:3005')
        .on('open', () => {
          const msg = {}
          testWs.send(JSON.stringify(msg))
          setTimeout(() => {
            expect(testPlugin.data.action).to.equal('message')
            expect(testPlugin.data.data).to.deep.equal(msg)
            res()
          }, 5)
        })
    })
  })

  it('throws an error on invalid key by default', () => {
    return new Promise(res => {
      const testWs = new WS('ws://localhost:3005')
        .on('open', () => {
          const msg = { key: 'invalid' }
          testWs.send(JSON.stringify(msg))
          setTimeout(() => {
            expect(testPlugin.data.action).to.equal('pluginError')
            expect(testPlugin.data.data).to.be.instanceof(Error)
            res()
          }, 5)
        })
    })
  })

  it('throws an error on invalid key when constructed with true', () => {
    return new Promise(res => {
      srv.plugins[1] = new KeyRouter(true, true)
      const testWs = new WS('ws://localhost:3005')
        .on('open', () => {
          const msg = { key: 'invalid' }
          testWs.send(JSON.stringify(msg))
          setTimeout(() => {
            expect(testPlugin.data.action).to.equal('pluginError')
            expect(testPlugin.data.data).to.be.instanceof(Error)
            res()
          }, 5)
        })
    })
  })

  it('does not throw an error on invalid key when constructed with false', () => {
    return new Promise(res => {
      srv.plugins[1] = new KeyRouter(true, false)
      const testWs = new WS('ws://localhost:3005')
        .on('open', () => {
          const msg = { key: 'invalid' }
          testWs.send(JSON.stringify(msg))
          setTimeout(() => {
            expect(testPlugin.data.action).to.equal('message')
            expect(testPlugin.data.data).to.deep.equal(msg)
            res()
          }, 5)
        })
    })
  })

  it('routes correctly', () => {
    return new Promise(res => {
      const router = srv.plugins[1] as KeyRouter
      const callArgs: any = {
        ws: null,
        msg: null
      }
      router.keys.validKey = (ws: WS, msg: WS.Data) => {
        callArgs.ws = ws
        callArgs.msg = msg
      }
      const testWs = new WS('ws://localhost:3005')
        .on('open', () => {
          const msg = { key: 'validKey', msg: 'Test message' }
          testWs.send(JSON.stringify(msg))
          setTimeout(() => {
            expect(callArgs.msg).to.deep.equal(msg)
            res()
          }, 5)
        })
    })
  })

  it('adds help key on true', () => {
    return new Promise(res => {
      const testWs = new WS('ws://localhost:3005')
        .on('open', () => {
          const msg = { key: 'help' }
          testWs.send(JSON.stringify(msg))
          testWs.on('message', (msg: string) => {
            const parsed = JSON.parse(msg)
            expect(parsed).to.be.instanceof(Array)
            expect(parsed).to.include('help')
            res()
          })
        })
    })
  })

  it('does not add help key on false', () => {
    return new Promise(res => {
      const router = new KeyRouter()
      router.helpKey = false
      srv.plugins[1] = router
      const testWs = new WS('ws://localhost:3005')
        .on('open', () => {
          const msg = { key: 'help' }
          testWs.send(JSON.stringify(msg))
          let called = false
          testWs.on('message', () => {
            called = true
          })
          setTimeout(() => {
            expect(called).that.equal(false)
            res()
          }, 5)
        })
    })
  })
})