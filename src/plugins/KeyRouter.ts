import * as WS from 'ws'
import { Plugin, pluginArgs } from '../'

export type handler = (ws: WS, msg: WS.Data) => void

// Best paired with JSON Parser plugin
export class KeyRouter implements Plugin {
  public keys: { [key: string]: handler } = {}
  public errorOnNoKey: boolean
  public errorOnInvalidKey: boolean

  public constructor(errorOnNoKey: boolean = true, errorOnInvalidKey: boolean = true, helpKey: boolean = true) {
    this.errorOnNoKey = errorOnNoKey
    this.errorOnInvalidKey = errorOnInvalidKey
    this.helpKey = helpKey
  }

  public handle(data: pluginArgs) {
    if (data.action !== 'message') {
      return
    }
    if (!data.data.key) {
      if (this.errorOnNoKey) {
        throw new Error(`No key in message body: ${ data.data }`)
      }
      return
    }
    if (!this.keys[data.data.key]) {
      if (this.errorOnInvalidKey) {
        throw new Error(`Invalid key: ${ data.data }`)
      }
      return
    }
    this.keys[data.data.key](data.ws, data.data)
  }

  public set helpKey(value: boolean) {
    if (value) {
      this.keys.help = (ws: WS) => {
        const keys = Object.keys(this.keys)
        ws.send(JSON.stringify(keys))
      }
    }
    else {
      delete this.keys.help
    }
  }

  public get helpKey() {
    return !!this.keys.help
  }
}