import 'mocha'
import { expect } from 'chai'

import { Logger } from './'

describe('Logger', () => {
  let logger: Logger
  let error: any
  let log: any

  beforeEach(() => {
    logger = new Logger()
    logger.logFuncs.error = (msg: any) => {
      error = msg
    }
    logger.logFuncs.log = (msg: any) => {
      log = msg
    }
  })

  afterEach(() => {
    error = null
    log = null
  })

  it('on error, calls error logFunc', () => {
    const data: any = {
      action: 'Error',
      data: '',
      ws: null
    }
    logger.handle(data)
    expect(error).to.deep.equal(data)
  })

  it('on not error, calls log logFunc', () => {
    const data: any = {
      action: 'not.error',
      data: '',
      ws: null
    }
    logger.handle(data)
    expect(log).to.deep.equal(data)
  })
})