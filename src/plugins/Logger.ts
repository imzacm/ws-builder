import { Plugin, pluginArgs } from '../'

export class Logger implements Plugin {
  public logFuncs = {
    log: console.log,
    warn: console.warn,
    error: console.error
  }

  public handle(data: pluginArgs) {
    let logFunc: Function = this.logFuncs.log
    if (data.action.includes('Error')) {
      logFunc = this.logFuncs.error
    }
    logFunc(data)
  }
}